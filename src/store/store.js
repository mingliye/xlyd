import Vue from 'vue';
import Vuex from 'vuex';
import register from "./components/register_store.js"
import mapStore from "./components/map_store.js"
import myServiceStore from "./components/myService_store.js"
import bankChooseStore from "./components/bankChoose_store.js"
import friendRongStore from "./components/friendRong_store.js"
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
     register,
     mapStore:mapStore,
     myServiceStore,
     bankChooseStore,
     friendRongStore,
 }
})
