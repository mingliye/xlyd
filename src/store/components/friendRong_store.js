export default {
  state:{
      rongLoad:'',
      groupUserids:[],      //创建群组选择的好友ids
      groupUsers:[],        //创建群组选择的好友
  },
  mutations:{
      setRongLoad(state,payload){
        state.rongLoad = payload;
      },
      setGroupUserIds(state,payload){
        state.groupUserids = payload;
      },
      setGroupUsers(state,payload){
        state.groupUsers = payload;
      },
  },

}
