export default {
  state:{
      bankStore:'',   //选择的银行卡信息
  },
  mutations:{
      setBankStore(state,payload){    //设置选中银行卡
        state.bankStore = payload;
      }
  }
}
