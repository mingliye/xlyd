export default {
  state:{
    token:'',
  },
  mutations: {
    setToken(state, tokenVal) {
      state.token = tokenVal;
    }
  }
}
