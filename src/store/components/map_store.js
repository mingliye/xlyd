export default {
  state:{
      mapVal:'',
      matchData:'',     //我的活动
      serviceData:'',   //我的服务
      classifyData:'', //活动类型
      sportsLevData:'',  //运动水平
      statusData:'',     //服务状态

      orderPushData:'',   //发布需求的列表数据
  },
  mutations:{
      setMapStore(state,payload){
        state.mapVal = payload;
      },
      setMatchData(state,info) {
        state.matchData = info;
      },
      setServiceData(state,info) {
        state.serviceData = info;
      },
      setClassifyData(state,info) {
        state.classifyData = info;
      },
      setSportsLevData(state,info) {
        state.sportsLevData = info;
      },
      setStatusData(state,info) {
        state.statusData = info;
      },

      // 发布需求的列表数据存储
      setOrderPushData(state,info) {
        state.orderPushData = info;
      }
  },

}
