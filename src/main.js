// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueAwesomeSwiper from 'vue-awesome-swiper'
require('swiper/dist/css/swiper.css')
Vue.use(VueAwesomeSwiper)
Vue.config.productionTip = false

import store from './store/store.js'


/* 全局注册组件 */
import headerbox from '@/components/header_footer/header';
Vue.component('headerbox', headerbox)
import footerNav from '@/components/header_footer/footer';
Vue.component('footerNav', footerNav)
import noneList from '@/components/noneList/noneList';
Vue.component('noneList',noneList)

import {configUrl} from '@/utils/configUrl.js';
import {imgUrlLink} from '@/utils/util.js';
Vue.prototype.$imgUrlLink = imgUrlLink;
import {globalData} from '@/utils/globalData.js';
Vue.prototype.$globalData = globalData;

//开启debug模式
Vue.config.debug = true;

/**
*vconsole
*/
// import VConsole from 'vconsole/dist/vconsole.min.js' //import vconsole
// let vConsole = new VConsole() // 初始化

import axios from 'axios'
Vue.prototype.$ajax = axios
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
/**
解决sessionID不一致的关键
**/
// axios.defaults.withCredentials=true;

/*html5plus*/
import VueHtml5Plus from 'vue-html5plus';
Vue.use(VueHtml5Plus);

import MintUI from 'mint-ui';
import 'mint-ui/lib/style.css';
Vue.use(MintUI);
import {MessageBox,Toast} from 'mint-ui';
Vue.prototype.toast = Toast;
Vue.prototype.messageBox = MessageBox;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
router.afterEach((to,from,next) => {
  window.scrollTo(0,0);
});
