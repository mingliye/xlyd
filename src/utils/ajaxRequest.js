import qs from 'qs';
import axios from 'axios';

import {postRequest} from './network.js';
// 注册
export const registerPost = (param) => postRequest(configUrl.REGISTER_URL,param,'post');
// 登录
export const loginPost = (param) => postRequest(configUrl.LOGIN_URL,param,'post');
// 分类
export const getClassifyList = (param) => postRequest(configUrl.CLASSIFYLIST_URL,param,'get');
// 检查用户是否认证
export const checkUserAuth = (param) => postRequest(configUrl.CHECKUSERAUTH_URL,param,'post');
// 获取可选银行
export const bankList = (param) => postRequest(configUrl.BANKLIST_URL,param,'post');
// 首页 热门推荐
export const matchHotIndex = (param) => postRequest(configUrl.MATCHHOTINDEX_URL,param,'post');
// 首页 好友进行时
export const matchFriendHand = (param) => postRequest(configUrl.MATCHFRIENDHAND_URL,param,'post');
// 首页 教练推荐（活动达人）
export const authHotUser = (param) => postRequest(configUrl.AUTHHOTUSER_URL,param,'post');
// 注册 发送短信获取验证码
export const registerCode = (param) => postRequest(configUrl.REGISTSENDCODE_URL,param,'post');
// 修改密码发送短信
export const updatePwdCode = (param) => postRequest(configUrl.UPDATEPWDSENDCODE_URL,param,'post');

/*
支付方式
*/
// 微信预支付
export const wxPay = (param) => postRequest(configUrl.WXPAY_URL,param,'post');
// 支付宝预支付
export const aliPay = (param) => postRequest(configUrl.ALIPAY_URL,param,'post');

/*
账户信息
*/
// 账户信息
export const userAccountInfo = (param) => postRequest(configUrl.USERACCOUNTINFO_URL,param,'post');
// 银行卡列表
export const userCardList = (param) => postRequest(configUrl.USERCARDLIST_URL,param,'post');
// 绑定银行卡
export const userCardBind = (param) => postRequest(configUrl.USERCARDBIND_URL,param,'post');
// 解绑银行卡
export const userCardUnbind = (param) => postRequest(configUrl.USERCARDUNBIND_URL,param,'post');
// 提现申请
export const userDrawApply = (param) => postRequest(configUrl.USERDRAWAPPLY_URL,param,'post');
// 提现记录
export const userDrawList = (param) => postRequest(configUrl.USERDRAWLIST_URL,param,'post');
// 账户明细
export const userAccountFlow = (param) => postRequest(configUrl.USERACCOUNTFLOW_URL,param,'post');
// 创建账户充值订单
export const userAccountRecharge = (param) => postRequest(configUrl.USERACCOUNTRECHARGE_URL,param,'post');

/*
用户模块
*/
// 用户信息详情
export const getUserInfo = (param) => postRequest(configUrl.USERINFO_URL,param,'post');
// 更新用户信息
export const updateUserInfo = (param) => postRequest(configUrl.UPDATEUSERINFO_URL,param,'post');
// 我的服务列表
export const skillList = (param) => postRequest(configUrl.SKILLLIST_URL,param,'post');
// 教练认证
export const authApply = (param) => postRequest(configUrl.AUTHAPPLY_URL,param,'post');
// 发布技能
export const skillPush = (param) => postRequest(configUrl.SKILLPUSH_URL,param,'post');
// 我的爱好列表
export const userHobby = (param) => postRequest(configUrl.USERHOBBY_URL,param,'post');
// 设置我的爱好
export const userSetHobby = (param) => postRequest(configUrl.USERSETHOBBY_URL,param,'post');
// 我的行程
export const userTrip = (param) => postRequest(configUrl.USERTRIP_URL,param,'post');

/*
赛事模块
*/
// 新增活动数据（我的活动）
export const matchPush = (param) => postRequest(configUrl.MATCHPUSH_URL,param,'post');
 // 获取我发布的赛事
 export const matchMyPush = (param) => postRequest(configUrl.MATCHMYPUSHED_URL,param,'post');
  // 获取我参加的赛事
  export const matchMyPartake = (param) => postRequest(configUrl.MATCHMYPARTAKE,param,'get');
  // 赛事详情
  export const matchInfomation = (param) => postRequest(configUrl.MATCHINFO_URL,param,'post');
  // 主页赛事列表
  export const matchListIndex = (param) => postRequest(configUrl.MATCHLISTINDEX_URL,param,'post');
  // 赛事点赞  取消点赞
  export const matchPraise = (param) => postRequest(configUrl.MATCHPRAISE_URL,param,'post');
  // 参加赛事
  export const matchPartake = (param) => postRequest(configUrl.MATCHPARTAKE_URL,param,'post');
  // 赛事评论列表
  export const matchCommentList = (param) => postRequest(configUrl.MATCHCOMMENTLIST_URL,param,'get');
  // 赛事评论列表
  export const matchCommentPush = (param) => postRequest(configUrl.MATCHCOMMENTPUSH_URL,param,'post');
  // 找赛事列表
  export const matchList = (param) => postRequest(configUrl.MATCHLIST_URL,param,'post');

  /*
  圈子模块
  */
  // 发布圈子文章
  export const circleArticlePush = (param) => postRequest(configUrl.CIRCLEARTICLEPUSH_URL,param,'post');
  // 我的朋友圈好友最新圈子信息
  export const circleArticleList = (param) => postRequest(configUrl.CIRCLEARTICLELIST_URL,param,'post');
  // 圈子文章详情
  export const circleArticleInfo = (param) => postRequest(configUrl.CIRCLEARTICLEINFO_URL,param,'post');
  // 圈子文章评论（回复）
  export const circleArticleCommentPush = (param) => postRequest(configUrl.CIRCLEARTICLECOMMENTPUSH_URL,param,'post');
  // 圈子文章点赞、取消点赞
  export const circleArticlePraise = (param) => postRequest(configUrl.CIRCLEARTICLEPRAISE_URL,param,'post');
  // 文章回复内容点赞
  export const circleCommentPraise = (param) => postRequest(configUrl.CIRCLECOMMENTPRAISE_URL,param,'post');


  /*
  我的好友
  */
  // 我的好友列表
  export const circleFriendList = (param) => postRequest(configUrl.CIRCLEFRIENDLIST_URL,param,'post');
  // 根据用户昵称 或用户编码 模糊检索 用户列表
  export const circleUserSearch = (param) => postRequest(configUrl.CIRCLEUSERSEARCH_URL,param,'post');
  // 添加好友申请
  export const circleApply = (param) => postRequest(configUrl.CIRCLEAPPLY_URL,param,'post');
  // 好友申请 列表
  export const circleApplyList = (param) => postRequest(configUrl.CIRCLEAPPLYLIST_URL,param,'post');
  // 我的群组列表
  export const circleMyGroup = (param) => postRequest(configUrl.CIRCLEMYGROUP_URL,param,'post');
  // 创建群组
  export const circleGroupCreate = (param) => postRequest(configUrl.CIRCLEGROUPCREATE_URL,param,'post');
  // 更新群组信息
  export const circleGroupUpdate = (param) => postRequest(configUrl.CIRCLEGROUPUPDATE_URL,param,'post');
  // 将好友加入群组
  export const circleGroupJoin = (param) => postRequest(configUrl.CIRCLEGROUPJOIN_URL,param,'post');
  // 退出群组
  export const circleGroupQuit = (param) => postRequest(configUrl.CIRCLEGROUPQUIT_URL,param,'post');
  // 解散群组
  export const circleGroupDismiss = (param) => postRequest(configUrl.CIRCLEGROUPDISMISS_URL,param,'post');
  // 查询群成员信息
  export const circleGroupInfo = (param) => postRequest(configUrl.CIRCLEGROUPINFO_URL,param,'post');
  // 同意申请 或拒绝好友申请
  export const circleShipOper = (param) => postRequest(configUrl.CIRCLESHIPOPER_URL,param,'post');
  // 删除好友
  export const circleFriendDel = (param) => postRequest(configUrl.CIRCLEFRIENDDEL_URL,param,'post');
  // 根据自己的爱好获取推荐的或用技能信息
  export const circleUserRecommend = (param) => postRequest(configUrl.CIRCLEUSERRECOMMEND_URL,param,'post');

  /*
  订单模块
  */
  // 发布需求
  export const orderPush = (param) => postRequest(configUrl.ORDERPUSH_URL,param,'post');
  // 查找需求列表
  export const orderListPending = (param) => postRequest(configUrl.ORDERLISTPENDING_URL,param,'post');

  // 我的订单列表
  // 我预约的 page_type =0
  // 预约我的 page_type =1 时传 orderType（0：需求订单 1：技能订单）
  export const orderListActive = (param) => postRequest(configUrl.ORDERLISTACTIVE_URL,param,'post');
  // 取消订单——我预约的情况
  export const orderCancle = (param) => postRequest(configUrl.ORDERCANCLE_URL,param,'post');
  // 取消订单、同意预约——预约我的情况
  export const orderCancleOper = (param) => postRequest(configUrl.ORDERCANCLE_OPER_URL,param,'post');
  // 需求列表——立即接单
  export const orderRobbing = (param) => postRequest(configUrl.ORDERROBBING_URL,param,'post');
  // 需求者  同意接单
  export const orderDemandAgree = (param) => postRequest(configUrl.ORDERDEMANDAGREE_URL,param,'post');
  // 确认接单（接下指定为我的订单）
  export const orderReceipt = (param) => postRequest(configUrl.ORDERRECEIPT_URL,param,'post');
  // 完成订单
  export const orderFinish = (param) => postRequest(configUrl.ORDERFINISH_URL,param,'post');
