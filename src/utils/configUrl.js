

import {globalData} from '@/utils/globalData.js';
let BASE_URL = globalData.BASE_URL;

global.configUrl = {
  /*
  公共接口
  */
  // 登录
  // LOGIN_URL: 'http://192.168.0.62:8080/shop-admin/mobile/user_login.htm',
  LOGIN_URL: BASE_URL + '/mobile/user_login.htm',
  // 注册
  REGISTER_URL: BASE_URL + '/mobile/user_register.htm',
  // 文件上传
  UPLOADFILES_URL: BASE_URL + '/mobile/uploadFiles.htm',
  // 获取可选银行
  BANKLIST_URL: BASE_URL + '/mobile/bank_list.htm',
  // 获取类别列表
  CLASSIFYLIST_URL: BASE_URL + '/mobile/classify_list.htm',
  // 检查用户是否认证
  CHECKUSERAUTH_URL: BASE_URL + '/mobile/check_user_auth.htm',
  // 首页 热门推荐
  MATCHHOTINDEX_URL: BASE_URL + '/mobile/match_hot_index.htm',
  // 首页 好友进行时
  MATCHFRIENDHAND_URL: BASE_URL + '/mobile/match_friend_hand.htm',
  // 首页 教练推荐（活动达人）
  AUTHHOTUSER_URL: BASE_URL + '/mobile/auth_hot_user.htm',
  // 注册  发送短信获取验证码
  REGISTSENDCODE_URL: BASE_URL + '/mobile/regist_sendCode.htm',
  // 修改密码发送短信
  UPDATEPWDSENDCODE_URL: BASE_URL + '/mobile/updatePwd_sendCode.htm',

  /*
  支付方式——预下单
  */
  // 微信预下单
  WXPAY_URL: BASE_URL + '/mobile/wx_pay.htm',
  // 支付宝预下单
  ALIPAY_URL: BASE_URL + '/mobile/alipay_pay.htm',

  /*
  账户信息
  */
  // 账户信息---账户余额
  USERACCOUNTINFO_URL: BASE_URL + '/mobile/user_account_info.htm',
  // 银行卡列表
  USERCARDLIST_URL: BASE_URL + '/mobile/user_card_list.htm',
  // 用户绑定银行卡
  USERCARDBIND_URL: BASE_URL + '/mobile/user_card_binding.htm',
  // 解绑银行卡
  USERCARDUNBIND_URL: BASE_URL + '/mobile/user_card_unbinding.htm',
  // 提现申请
  USERDRAWAPPLY_URL: BASE_URL + '/mobile/user_draw_apply.htm',
  // 提现记录
  USERDRAWLIST_URL: BASE_URL + '/mobile/user_draw_list.htm',
  // 账户明细
  USERACCOUNTFLOW_URL: BASE_URL + '/mobile/user_account_flow.htm',
  // 创建账户充值订单
  USERACCOUNTRECHARGE_URL: BASE_URL + '/mobile/user_account_recharge.htm',

  /*
  用户模块
  */
  // 朋友圈
  // SEARCHCIRCLEUSER_URL: BASE_URL + '/mobile/circle_user_search.htm',
  // 用户信息详情
  USERINFO_URL: BASE_URL + '/mobile/user_info.htm',
  // 更新用户信息
  UPDATEUSERINFO_URL: BASE_URL + '/mobile/user_info_update.htm',
  // 教练认证
  AUTHAPPLY_URL: BASE_URL + '/mobile/auth_apply.htm',
  // 我的服务列表
  SKILLLIST_URL: BASE_URL + '/mobile/skill_list.htm',
  // 发布技能
  SKILLPUSH_URL: BASE_URL + '/mobile/skill_push.htm',
  // 我的爱好列表
  USERHOBBY_URL: BASE_URL + '/mobile/user_hobby.htm',
  // 设置我的爱好
  USERSETHOBBY_URL: BASE_URL + '/mobile/user_set_hobby.htm',
  // 我的行程
  USERTRIP_URL: BASE_URL + '/mobile/user_trip.htm',

  /*
  赛事模块
  */
  // 新增活动数据（发布赛事）
  MATCHPUSH_URL: BASE_URL + '/mobile/match_push.htm',
  // 获取我发布的赛事
  MATCHMYPUSHED_URL: BASE_URL + '/mobile/match_my_pushed.htm',
  // 获取我参加的赛事
  MATCHMYPARTAKE: BASE_URL + '/mobile/match_my_partake.htm',
  // 赛事详情
  MATCHINFO_URL: BASE_URL + '/mobile/match_info.htm',
  // 主页赛事列表
  MATCHLISTINDEX_URL: BASE_URL + '/mobile/match_list_index.htm',
  // 赛事点赞 / 取消点赞
  MATCHPRAISE_URL: BASE_URL + '/mobile/match_praise.htm',
  // 我要参加赛事
  MATCHPARTAKE_URL: BASE_URL + '/mobile/match_partake.htm',
  // 获取赛事活动评论列表
  MATCHCOMMENTLIST_URL: BASE_URL + '/mobile/match_comment_list.htm',
  // 赛事评论发表
  MATCHCOMMENTPUSH_URL: BASE_URL + '/mobile/match_comment_push.htm',
  // 找赛事
  MATCHLIST_URL: BASE_URL + '/mobile/match_list.htm',

  /*
  圈子模块
  */
  // 发布圈子文章
  CIRCLEARTICLEPUSH_URL: BASE_URL + '/mobile/circle_article_push.htm',
  // 我的朋友圈好友最新圈子信息
  CIRCLEARTICLELIST_URL: BASE_URL + '/mobile/circle_article_list.htm',
  // 圈子文章详情
  CIRCLEARTICLEINFO_URL: BASE_URL + '/mobile/circle_article_info.htm',
  // 圈子文章 点赞or取消点赞
  CIRCLEARTICLEPRAISE_URL: BASE_URL + '/mobile/circle_article_praise.htm',
  // 圈子文章评论（回复）
  CIRCLEARTICLECOMMENTPUSH_URL: BASE_URL + '/mobile/circle_article_comment_push.htm',
  // 文章回复内容点赞
  CIRCLECOMMENTPRAISE_URL: BASE_URL + '/mobile/circle_comment_praise.htm',

  /*
  我的好友
  */
  // 我的好友列表
  CIRCLEFRIENDLIST_URL: BASE_URL + '/mobile/circle_friend_list.htm',
  // 根据用户昵称 或用户编码 模糊检索 用户列表
  CIRCLEUSERSEARCH_URL: BASE_URL + '/mobile/circle_user_search.htm',
  // 添加好友申请
  CIRCLEAPPLY_URL: BASE_URL + '/mobile/circle_apply.htm',
  // 申请列表
  CIRCLEAPPLYLIST_URL: BASE_URL + '/mobile/circle_apply_list.htm',
  // 我的群组列表
  CIRCLEMYGROUP_URL: BASE_URL + '/mobile/circle_my_group.htm',
  // 查询群成员信息
  CIRCLEGROUPINFO_URL: BASE_URL + '/mobile/circle_group_info.htm',
  // 创建群组（至少选择2个成员)
  CIRCLEGROUPCREATE_URL: BASE_URL + '/mobile/circle_group_create.htm',
  // 更新群组信息
  CIRCLEGROUPUPDATE_URL: BASE_URL + '/mobile/circle_group_update.htm',
  // 将好友加入群组
  CIRCLEGROUPJOIN_URL: BASE_URL + '/mobile/circle_group_join.htm',
  // 退出群组
  CIRCLEGROUPQUIT_URL: BASE_URL + '/mobile/circle_group_quit.htm',
  // 解散群组
  CIRCLEGROUPDISMISS_URL: BASE_URL + '/mobile/circle_group_dismiss.htm',
  // 同意申请 或 拒绝
  CIRCLESHIPOPER_URL: BASE_URL + '/mobile/circle_ship_oper.htm',
  // 删除好友
  CIRCLEFRIENDDEL_URL: BASE_URL + '/mobile/circle_friend_del.htm',
  // 根据自己的爱好获取推荐的或用技能信息（推荐好友）
  CIRCLEUSERRECOMMEND_URL: BASE_URL + '/mobile/circle_user_recommend.htm',

  /*
  订单模块
  */
  // 发布需求（指定服务者技能下单——不指定服务者）
  ORDERPUSH_URL: BASE_URL + '/mobile/order_push.htm',
  // 查找需求列表
  ORDERLISTPENDING_URL: BASE_URL + '/mobile/order_list_pending.htm',
  // 预约列表
  /*
  我的订单列表
  我预约的 page_type =0
  预约我的 page_type =1 时传 orderType（0：需求订单 1：技能订单）
  */
  ORDERLISTACTIVE_URL: BASE_URL + '/mobile/order_list_active.htm',
  // 取消订单申请——————我预约的情况
  ORDERCANCLE_URL: BASE_URL + '/mobile/order_cancel.htm',
  // (取消操作)同意取消 -- 拒绝取消———————————预约我的情况
  ORDERCANCLE_OPER_URL: BASE_URL + '/mobile/order_cancel_oper.htm',
  // 需求列表——立即接单
  ORDERROBBING_URL: BASE_URL + '/mobile/order_robbing.htm',
  // （需求者）同意接单
  ORDERDEMANDAGREE_URL: BASE_URL + '/mobile/order_demand_agree.htm',
  // 确认接单（接下指定为我的订单）
  ORDERRECEIPT_URL: BASE_URL + '/mobile/order_receipt.htm',
  // 完成订单
  ORDERFINISH_URL: BASE_URL + '/mobile/order_finish.htm',
}
