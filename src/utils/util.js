

import {globalData} from '@/utils/globalData.js';
// 跳转到子页面
export const toChildrenPage = (self,pageName) => {
  self.$router.push({name:pageName});
}

// 图片地址拼接
export const imgUrlLink = (imgurl) => {
  return globalData.BASE_URL + imgurl;
}

// 手机号正则判断
export const mobileTest = (phone) => {
  console.log(phone);
  let reg = /^1\d{10}$/;    //规则：11位数字，以1开头
  if(reg.test(phone)) {
    return true;
  }else{
    return false;
  }
}

// 日期格式转换 yyyy-MM-DD HH:mm:ss
export const dateChangeTime = (timer) => {
  let _time = new Date(timer);
  let yyyy = _time.getFullYear();
  let MM = _time.getMonth()+1 >= 10 ? _time.getMonth()+1 : '0' + (_time.getMonth()+1);
  let DD = _time.getDate() >= 10 ? _time.getDate() : '0' + _time.getDate();
  let hh = _time.getHours() >= 10 ? _time.getHours() : '0' + _time.getHours();
  let mm = _time.getMinutes() >= 10 ? _time.getMinutes() : '0' + _time.getMinutes();
  let ss = _time.getSeconds() >= 10 ? _time.getSeconds() : '0' + _time.getSeconds();
  return yyyy+'-'+MM+'-'+DD+' '+hh+':'+mm+':'+ss;
}
