import Vue from 'vue'
import router from '../router'
import qs from 'qs';
import axios from 'axios';
import {MessageBox,Toast} from 'mint-ui';

export const postRequest = (url,param,requestType) => {
  console.log(url);
  return axios({
    method: requestType,
    url:url,
    params:param,
    paramsSerializer: function(param){
      return qs.stringify(param)
    },
  })
}

// 拦截器
axios.interceptors.response.use( response => {
  console.log(response);
  // response.header("Access-Control-Allow-Credentials", "true");
  // response.header('Access-Control-Allow-Origin', 'http://localhost:8080');
  // response.header('Access-Control-Allow-Credentials', true);
  if(response.data.code==200){
    return response;
  }else {
    console.log('错误:'+response.data.code);
    switch (response.data.code) {
      case 404:
        router.push({name:'login'});
        break;
      default:

    }
    Toast(response.data.msg);
  }
},error => {
  console.log(error);
  if(error.response) {
    console.log(error.response);
  }
});

// let xlyd_loginInfo = window.localStorage.getItem('xlyd_loginInfo');
// if (!xlyd_loginInfo) {
//   MessageBox.alert('您还未登录，请先登录').then(action=>{
//     router.push({name:'login'});
//   });
// }
// axios.interceptors.response.use(function (response) {
// 	// token 已过期，重定向到登录页面
// 	if (response.data.code == 404){
// 		// localStorage.clear()
// 		router.replace({
//                         path: '/signin',
//                         query: {redirect: router.currentRoute.fullPath}
//                     })
// 	}
// 	return response
// }, function (error) {
// 	// Do something with response error
// 	return Promise.reject(error)
// })
