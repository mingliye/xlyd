import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: ()=>import('@/pages/index/index')
    },
    {
      path: '/login',
      name: 'login',
      component: ()=>import('@/pages/login/login')
    },
    {
      path: '/register',
      name: 'register',
      component: ()=>import('@/pages/login/register')
    },
    {
      path: '/baiduMap',
      name: 'baiduMap',
      component: ()=>import('@/components/baiduMap/baiduMap')
    },
    {
      path: '/members',
      name: 'members',
      component: ()=>import('@/pages/members/memberCenter')
    },
    {
      path: '/myWallet',
      name: 'myWallet',
      component: ()=>import('@/pages/members/myWallet/myWallet'),
      children:[{
        path: 'reCharge',
        name: 'reCharge',
        component: ()=>import('@/pages/members/myWallet/reCharge/reCharge'),
      },{
        path: 'withdrawCash',
        name: 'withdrawCash',
        component: ()=>import('@/pages/members/myWallet/withdrawCash/withdrawCash'),
      },{
        path: 'banklist',
        name: 'banklist',
        component: ()=>import('@/pages/members/myWallet/banklist/banklist'),
        children:[{
          path: 'addBank',
          name: 'addBank',
          component: ()=>import('@/pages/members/myWallet/banklist/addBank'),
        }]
      },
      {
        path: 'modifyBanklist',
        name: 'modifyBanklist',
        component: ()=>import('@/pages/members/myWallet/bankList/modifyBanklist'),
      },
      {
        path: 'withdrawList',
        name: 'withdrawList',
        component: ()=>import('@/pages/members/myWallet/withdrawList/withdrawList'),
      },
      {
        path: 'walletDetail',
        name: 'walletDetail',
        component: ()=>import('@/pages/members/myWallet/walletDetail/walletDetail'),
        children:[{
          path:'income',
          name:'income',
          component: ()=>import('@/pages/members/myWallet/walletDetail/income'),
        },
        {
          path:'defray',
          name:'defray',
          component: ()=>import('@/pages/members/myWallet/walletDetail/defray'),
        }]
      }]
    },
    // 我的活动
    {
      path: '/myActivity',
      name: 'myActivity',
      component: ()=>import('@/pages/members/myActivity/myActivity'),
      children:[{
        path:'activityPublish',
        name:'activityPublish',
        component: ()=>import('@/pages/members/myActivity/activityPublish'),
      }]
    },
    // 我的行程
    {
      path: '/myScheduling',
      name: 'myScheduling',
      component: ()=>import('@/pages/members/myScheduling/myScheduling'),
      children:[{
        path:'schedulPublish',
        name:'schedulPublish',
        component: ()=>import('@/pages/members/myScheduling/schedulPublish'),
      }]
    },
    // 个人简历
    {
      path: '/curriculumVitae',
      name: 'curriculumVitae',
      component: ()=>import('@/pages/members/curriculumVitae/curriculumVitae'),

    },
    {
      path: '/addressManage',
      name: 'addressManage',
      component: ()=>import('@/pages/members/addressManage/addressManage'),
      children: [{
        path: 'addAddress',
        name: 'addAddress',
        component: ()=>import('@/pages/members/addressManage/addAddress/addAddress'),
      }]
    },
    {
      path: '/myAppointment',
      name: 'myAppointment',
      component: ()=>import('@/pages/members/myAppointment/myAppointment'),
    },
    // 我的服务
    {
      path: '/myService',
      name: 'myService',
      component: ()=>import('@/pages/members/myService/myService'),
      children:[{
        path:'myServiceType',
        name:'myServiceType',
        component:()=>import('@/pages/members/myService/myServiceType/myServiceType')
      }]
    },
    {
      path: '/publishService',
      name: 'publishService',
      component: ()=>import('@/pages/members/myService/publishService/publishService'),
    },
    // 身份认证
    {
      path: '/authentication',
      name: 'authentication',
      component: ()=>import('@/pages/members/authentication/authentication'),
    },
    // 找教练、找陪练、找圈友
    {
      path: '/findService',
      name: 'findService',
      component: ()=>import('@/pages/members/myService/findService/findServiceType'),
    },
    // 发布需求
    {
      path: '/pushNeed',
      name: 'pushNeed',
      component: ()=>import('@/pages/members/myService/findService/pushNeed/pushNeed'),
    },
    // 预约下单
    {
      path: '/reservation',
      name: 'reservation',
      component: ()=>import('@/pages/members/myService/findService/reservation/reservation'),
    },
    {
      path: '/myFriends',
      name: 'myFriends',
      component: ()=>import('@/pages/members/myFriends/myFriends'),
    },
    {
      path: '/addFriend',
      name: 'addFriend',
      component: ()=>import('@/pages/members/myFriends/addFriends/addFriend'),
    },
    // 搜索用户
    {
      path: '/searchFriend',
      name: 'searchFriend',
      component: ()=>import('@/pages/members/myFriends/searchFriend/searchFriend'),
    },
    // 好友申请
    {
      path: '/applyFriend',
      name: 'applyFriend',
      component: ()=>import('@/pages/members/applyFriend/applyFriend'),
    },
    {
      path: '/friendDetail',
      name: 'friendDetail',
      component: ()=>import('@/pages/members/myFriends/friendDetail/friendDetail'),
    },
    // 我的群组
    {
      path: '/myGroup',
      name: 'myGroup',
      component: ()=>import('@/pages/members/myGroup/myGroup'),
      children:[{
        path: '/addGroup',
        name: 'addGroup',
        component: ()=>import('@/pages/members/myGroup/addGroup/addGroup'),
        children:[{
          // 群组添加成员
          path: '/chooseFriends',
          name: 'chooseFriends',
          component: ()=>import('@/pages/members/myGroup/addGroup/chooseFriends'),
        }]
      }]
    },
    // 兴趣爱好
    {
      path: '/myInterest',
      name: 'myInterest',
      component: ()=>import('@/pages/members/myInterest/myInterest'),
    },{
      path: '/myMessage',
      name: 'myMessage',
      component: ()=>import('@/pages/members/myMessage/myMessage'),
    },
    // 赛事
    {
      path: '/events',
      name: 'events',
      component: ()=>import('@/pages/events/events')
    },
    {
      path: '/eventDetail',
      name: 'eventDetail',
      component: ()=>import('@/pages/events/eventDetail/eventDetail')
    },
    // 赛事评论
    {
      path: '/eventMessage',
      name: 'eventMessage',
      component: ()=>import('@/pages/events/eventMessage/playMessage')
    },
    {
      path: '/eventmsgCon',
      name: 'eventmsgCon',
      component: ()=>import('@/pages/events/eventMessage/msgCon')
    },
    {
      path: '/playCircle',
      name: 'playCircle',
      component: ()=>import('@/pages/playCircle/playCircle'),
      children:[{
        path: 'publishCircle',
        name: 'publishCircle',
        component: ()=>import('@/pages/playCircle/publishCircle/publishCircle')
      }]
    },
    {
      path: '/playMessage',
      name: 'playMessage',
      component: ()=>import('@/pages/playCircle/playMessage/playMessage')
    },
    {
      path: '/msgCon',
      name: 'msgCon',
      component: ()=>import('@/pages/playCircle/playMessage/msgCon')
    },
    // 找赛事
    {
      path: '/findMatch',
      name: 'findMatch',
      component: ()=>import('@/pages/members/myService/findService/findMatch')
    },
    // 支付方式
    {
      path: '/paymentType',
      name: 'paymentType',
      component: ()=>import('@/pages/paymentType/paymentType')
    },
    // 融云通讯
    {
      path: '/rongyunChat',
      name: 'rongyunChat',
      component: ()=>import('@/pages/rongyunChat/rongyunChat')
    },
    // 融云群聊
    {
      path: '/rongyunGroup',
      name: 'rongyunGroup',
      component: ()=>import('@/pages/rongyunChat/rongyunGroup')
    },
    // 融云群聊——群组详情
    {
      path: '/groupInfo',
      name: 'groupInfo',
      component: ()=>import('@/pages/members/myGroup/groupInfo')
    },

    /**
    *商城部分开始
    */
    {
      path: '/mallIndex',
      name: 'mallIndex',
      component: ()=>import('@/sportsMall/index/index')
    },
    {
      path: '/mallClassification',
      name: 'mallClassification',
      component: ()=>import('@/sportsMall/classification/classification')
    },
  ]
})
